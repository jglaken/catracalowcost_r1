/*
 * blinkLeds.cpp
 *
 *  Created on: Jan 24, 2020
 *      Author: juanjose
 */

#include "blinkLeds.hpp"

BlinkLeds::BlinkLeds(GPIO_TypeDef *_port, uint16_t _pin)
{
	port = _port;
	pin = _pin;
	ledOff();
}

void BlinkLeds::ledOn()
{
	HAL_GPIO_WritePin(port, pin, GPIO_PIN_SET);
}

void BlinkLeds::ledOff()
{
	HAL_GPIO_WritePin(port, pin, GPIO_PIN_RESET);
}

void BlinkLeds::toggleLed(uint16_t toggleTime)
{
	HAL_GPIO_TogglePin(port, pin);
	HAL_Delay(toggleTime);
}

