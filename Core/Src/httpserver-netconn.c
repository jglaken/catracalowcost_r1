/**
  ******************************************************************************
  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/httpser-netconn.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-November-2015
  * @brief   Basic http server implementation using LwIP netconn API
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
//#include "json-c/json.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip.h"
#include "string.h"
#include "httpserver-netconn.h"
#include "cmsis_os.h"
#include "jsmn.h"
//#include "../webpages/index.h"
//#include "temp.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define WEBSERVER_THREAD_PRIO    ( tskIDLE_PRIORITY + 4 )

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u32_t nPageHits = 0;
extern ETH_HandleTypeDef heth;
UART_HandleTypeDef huart3;
HAL_StatusTypeDef return_val;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* Private Json estructure ---------------------------------------------------*/
static const char *JSON_STRING =
		"{\"led1\": \"LedGreen\", \"state\":true}";

static int jsoneq(const char *json, jsmntok_t *tok, const char *s){
	if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
	      strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
	    return 0;
	  }
	  return -1;
}

/**
  * @brief serve tcp connection
  * @param conn: pointer on connection structure
  * @retval None
  */
void http_server_serve(struct netconn *conn)
{
  struct netbuf *inbuf;
  err_t recv_err;
  char* buf;
  u16_t buflen;

  const char *m1 = "value=1";
  const char *metodo1;

  int i, r;
  jsmn_parser p;
  jsmntok_t t[32];		// nao espera mais de 32 tokens
  jsmn_init(&p);
  r = jsmn_parse(&p, JSON_STRING, strlen(JSON_STRING), t, sizeof(t) / sizeof(t[0]));

  /* Read the data from the port, blocking if nothing yet there.
   We assume the request (the part we care about) is in one netbuf */
  recv_err = netconn_recv(conn, &inbuf);

  if (recv_err == ERR_OK)
  {
    if (netconn_err(conn) == ERR_OK)
    {
      netbuf_data(inbuf, (void**)&buf, &buflen);

      /* Is this an HTTP GET command? (only check the first 5 chars, since
      there are other formats for GET, and we're keeping it very simple )*/
      if ((buflen >=5) && (strncmp(buf, "GET /", 5) == 0))
      {
    	  /* ******************************************* Test 1 Padronizacao API **********************************************/

//    	  metodosHttp(buf);
//    	  ptrToken1 = strtok(buf, "?");
//    	  while (ptrToken1 != NULL){
//    		  sprintf(metodo1, "%s\n", ptrToken1);
//    		  ptrToken1 = strtok(NULL, " ");
//    	  }
//    	  HAL_UART_Transmit(&huart3, metodo1, 50, 1000);


    	  /* ******************************************* Test 2 Padronizacao API **********************************************/

//    	  if (strncmp((char const *)buf,"GET /led1?value=1", 17) == 0) {
//			  HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, GPIO_PIN_SET);
//			  netconn_write(conn, (const unsigned char*)"{\nLed 1: True\n}", 16, NETCONN_NOCOPY);
//		  }
//    	  else if (strncmp((char const *)buf,"GET /led1?value=0", 17) == 0) {
//    		  HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, GPIO_PIN_RESET);
//    		  netconn_write(conn, (const unsigned char*)"{\nLed 1: False\n}", 17, NETCONN_NOCOPY);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /led2", 9) == 0) {
//    		  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
//    		  netconn_write(conn, (const unsigned char*)"{\nTOGGLE LED BLUE\n}", 19, NETCONN_NOCOPY);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /led3", 9) == 0) {
//    		  HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
//    		  netconn_write(conn, (const unsigned char*)"{\nTOGGLE LED RED\n}", 18, NETCONN_NOCOPY);
//    	  }


    	  /* ******************************************* Test 3 Padronizacao API **********************************************/
//    	  if (strncmp((char const *)buf,"GET /led1", 9) == 0) {
//    		  HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
//			  netconn_write(conn, (const unsigned char*)"{\nLed 1: True\n}", 16, NETCONN_NOCOPY);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /led2", 9) == 0) {
//    		  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
//    		  netconn_write(conn, (const unsigned char*)"{\nTOGGLE LED BLUE\n}", 19, NETCONN_NOCOPY);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /led3", 9) == 0) {
//    		  HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
////    		  netconn_write(conn, (const unsigned char*)"{\nTOGGLE LED RED\n}", 18, NETCONN_NOCOPY);
//
//    		  for (i = 0; i < r; i++){
//    			  if (jsoneq(JSON_STRING, &t[i], "led1") == 0){
//    				  netconn_write(conn, t[i + 1].end - t[i + 1].start, 18, NETCONN_NOCOPY);
//    				  i++;
//    			  }
//    		  }
//    	  }


    	  /* ******************************************* Test 3 Padronizacao API **********************************************/
//    	  if (strncmp((char const *)buf,"GET /led1?value=", 16) == 0) {
//    		  metodo1 = strstr(buf, m1);
//    		  netconn_write(conn,metodo1, 8, NETCONN_NOCOPY);
//
////    		  if (metodo1 == "value=1") HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, GPIO_PIN_SET);
////    		  else HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, GPIO_PIN_RESET);
//    	  }


//    	  if (strncmp((char const *)buf,"GET /metodos/setup/macaddress",24)==0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nMAC ADDRESS: \n}", 17, NETCONN_NOCOPY);
//
////    		  HAL_UART_Transmit(&huart3, "MAC ADDRESS: ", 13, 100);
//    	  }
//
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/ipaddress", 23) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nIP ADDRESS: \n}", 16, NETCONN_NOCOPY);
//
////    		  HAL_UART_Transmit(&huart3, "IP ADDRESS: ", 13, 100);
////    		  HAL_UART_Transmit(&huart3, gnetif.ip_addr, 14, 100);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/subnetmask", 24) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nSUBNET MASK: \n}", 17, NETCONN_NOCOPY);
//
////    		  HAL_UART_Transmit(&huart3, "SUBNET MASK: ", 13, 100);
////    		  HAL_UART_Transmit(&huart3, netmask, 13, 100);
//
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/gateway", 21) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nGATEWAY: \n}", 13, NETCONN_NOCOPY);
//
////    		  HAL_UART_Transmit(&huart3, gw, 13, 100);
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/ipintegrafacil", 29) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nIP INTEGRA FACIL: \n}", 23, NETCONN_NOCOPY);
//
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/portintegrafacil", 24) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nPORT INTEGRA FACIL: {\n", 24, NETCONN_NOCOPY);
//
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/sensorports", 25) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nSENSOR PORTS: \n}", 18, NETCONN_NOCOPY);
//
//    	  }
//    	  else if (strncmp((char const *)buf,"GET /metodos/setup/relayports", 24) == 0) {
//    		  netconn_write(conn, (const unsigned char*)"{\nRELAY PORTS: \n}", 17, NETCONN_NOCOPY);
//    	  }
//


//    	  else if(strncmp((char const *)buf, "GET /rele1/1", 12) == 0){
//    		  HAL_GPIO_WritePin(PIN_MOD_GPIO_Port, PIN_MOD_Pin, GPIO_PIN_RESET);
//    		  HAL_GPIO_WritePin(RELE_1_GPIO_Port, RELE_1_Pin, GPIO_PIN_SET);
//    		  netconn_write(conn, (const unsigned char*)"{\nRELE 1: ON\n}", 14, NETCONN_NOCOPY);
//    	  }
//    	  else if(strncmp((char const *)buf, "GET /rele1/0", 12) == 0){
//    		  HAL_GPIO_WritePin(PIN_MOD_GPIO_Port, PIN_MOD_Pin, GPIO_PIN_RESET);
//    		  HAL_GPIO_WritePin(RELE_1_GPIO_Port, RELE_1_Pin, GPIO_PIN_RESET);
//    		  netconn_write(conn, (const unsigned char*)"{\nRELE 1: OFF\n}", 15, NETCONN_NOCOPY);
//    	  }


////    	  else if (strncmp((char const *)buf,"GET /btn1", 9) == 0) {
////    		  if(HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin) == GPIO_PIN_SET)
////    			  netconn_write(conn, (const unsigned char*)"{\nUSER BUTTON ON\n}", 18, NETCONN_NOCOPY);
////    		  else
////    			  netconn_write(conn, (const unsigned char*)"{\nUSER BUTTON OFF\n}", 19, NETCONN_NOCOPY);
////    	  }
////    	  else if (strncmp((char const *)buf,"GET /adc", 8) == 0) {
////    		  sprintf(buf, "%2.1f °C", getMCUTemperature());
////    		  netconn_write(conn, (const unsigned char*)buf, strlen(buf), NETCONN_NOCOPY);
////    	  }
      }
    }
  }
  /* Close the connection (server closes in HTTP) */
  netconn_close(conn);

  /* Delete the buffer (netconn_recv gives us ownership,
   so we have to make sure to deallocate the buffer) */
  netbuf_delete(inbuf);
}


/**
  * @brief  http server thread
  * @retval None
  */
void http_server_netconn_thread()
{
  struct netconn *conn, *newconn;
  err_t err, accept_err;

  /* Create a new TCP connection handle */
  conn = netconn_new(NETCONN_TCP);

  if (conn!= NULL)
  {
    /* Bind to port 80 (HTTP) with default IP address */
    err = netconn_bind(conn, NULL, 80);

    if (err == ERR_OK)
    {
      /* Put the connection into LISTEN state */
      netconn_listen(conn);

      while(1)
      {
        /* accept any icoming connection */
        accept_err = netconn_accept(conn, &newconn);
        if(accept_err == ERR_OK)
        {
          /* serve connection */
          http_server_serve(newconn);

          /* delete connection */
          netconn_delete(newconn);
        }
      }
    }
  }
}

//void metodosHttp(uint16_t urlMetodos)
//{
//	char *ptrToken;
//	char metodo1[50];
//
//	ptrToken = strtok(urlMetodos, "?");
//
//	while(ptrToken != NULL){
//		sprintf(metodo1, "%s\n", ptrToken);
//		ptrToken = strtok(NULL, " ");
//	}
//}
