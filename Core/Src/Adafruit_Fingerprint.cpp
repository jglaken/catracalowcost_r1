/*
 * Adafruit_Fingerprint.cpp
 *
 *  Created on: Jan 27, 2020
 *      Author: juanjose
 */

#include "Adafruit_Fingerprint.hpp"


#define SERIAL_WRITE HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

#define GET_CMD_PACKET(...) \
  uint8_t data[] = {__VA_ARGS__}; \
  Adafruit_Fingerprint_Packet packet(FINGERPRINT_COMMANDPACKET, sizeof(data), data); \
  writeStructuredPacket(packet); \
  if (getStructuredPacket(&packet) != FINGERPRINT_OK) return FINGERPRINT_PACKETRECIEVEERR; \
  if (packet.type != FINGERPRINT_ACKPACKET) return FINGERPRINT_PACKETRECIEVEERR;

#define SEND_CMD_PACKET(...) GET_CMD_PACKET(__VA_ARGS__); return packet.data[0];

/***************************************************************************
 PUBLIC FUNCTIONS
 ***************************************************************************/

Adafruit_Fingerprint::Adafruit_Fingerprint(UART_HandleTypeDef ss, uint32_t password)
{
	thePassword = password;

}

void Adafruit_Fingerprint::begin(uint32_t baud)
{
	HAL_UART_Receive(mySerial, recvPacket, 10, baud);
}

bool Adafruit_Fingerprint::verifyPassword(void)
{
	return checkPassword() == FINGERPRINT_OK;
}

uint8_t Adafruit_Fingerprint::checkPassword(void)
{
  GET_CMD_PACKET(FINGERPRINT_VERIFYPASSWORD,
                  (uint8_t)(thePassword >> 24), (uint8_t)(thePassword >> 16),
                  (uint8_t)(thePassword >> 8), (uint8_t)(thePassword & 0xFF));
  if (packet.data[0] == FINGERPRINT_OK)
    return FINGERPRINT_OK;
  else
    return FINGERPRINT_PACKETRECIEVEERR;
}

uint8_t Adafruit_Fingerprint::getImage(void)
{
	SEND_CMD_PACKET(FINGERPRINT_GETIMAGE);
}

void Adafruit_Fingerprint::writeStructuredPacket(const Adafruit_Fingerprint_Packet & packet)
{
//	SERIAL_WRITE(&huart2, packet.address[0], 10, 100);
//	SERIAL_WRITE(&huart2, packet.address[1], 10, 100);
//	SERIAL_WRITE(&huart2, packet.address[2], 10, 100);
//	SERIAL_WRITE(&huart2, packet.address[3], 10, 100);
//	SERIAL_WRITE(packet.type);
}

uint8_t Adafruit_Fingerprint::getStructuredPacket(Adafruit_Fingerprint_Packet * packet, uint16_t timeout)
{

}



















