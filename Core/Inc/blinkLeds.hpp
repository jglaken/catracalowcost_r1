/*
 * blinkLeds.hpp
 *
 *  Created on: Jan 24, 2020
 *      Author: juanjose
 */

#ifndef INC_BLINKLEDS_HPP_
#define INC_BLINKLEDS_HPP_

#include "stm32f2xx_hal.h"

class BlinkLeds{
	public:
		BlinkLeds(GPIO_TypeDef *_port, uint16_t _pin);
		void ledOn();
		void ledOff();
		void toggleLed(uint16_t toggleTime);

	private:
		GPIO_TypeDef *port;
		uint16_t pin;

};



#endif /* INC_BLINKLEDS_HPP_ */
