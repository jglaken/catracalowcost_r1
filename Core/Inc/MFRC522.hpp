/*
 * MFRC522.hpp
 *
 *  Created on: Feb 14, 2020
 *      Author: juanjose
 */

#ifndef INC_MFRC522_HPP_
#define INC_MFRC522_HPP_

#include "deprecated.h"
// Enable integer limits
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#ifndef MFRC522_SPICLOCK
#define MFRC522_SPICLOCK SPI_CLOCK_DIV4			// MFRC522 accept upto 10MHz
#endif


class MFRC522 {
public:
	MFRC522();


};

#endif /* INC_MFRC522_HPP_ */
